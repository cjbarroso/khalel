#+TITLE: =khalel= changelog

Below are the summarized changes made in each version:

* v0.1.7
- adds variable =khal= configuration file location: =khalel-khal-config=. The
  default is =nil= which uses =khal='s default location.
- adds configurable format for import of events: =khal-import-format=.
- it is now possible to limit the import of events to only the default calendar
  by calling =khal-import-upcoming-events= with a prefix argument.
- the header of the org file with imported events can be changed through the
  variable =khalel-import-org-file-header=
- allows editing events scheduled in the past
* v0.1.6
- improves error handling on import and capture
- add (rudimentary) verification of ics export results
- adds auto-update of upcoming events on capture
  - can be disabled via =khalel-update-upcoming-events-after-capture=
- improves handling of temporary buffers during capture
  - no switching to temporary buffer except in case of error
  - kill temporary buffer after capture when successful or aborted
- extends README with troubleshooting section
* v0.1.5
- improves handling of repeating events
- puts content of imported events into list format
- fix handling of multi-line locations
- reverts imported calendar buffer after update
* v0.1.4
- initial release (on MELPA)
